import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { Observable } from 'rxjs/Observable';
import { FirebaseProvider } from '../../providers/firebase/firebase';

/**
 * Generated class for the AddStreamerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-streamer',
  templateUrl: 'add-streamer.html',
})
export class AddStreamerPage {
  inputStreamer:string = "";
  foundChannels:Observable<any>

  constructor(public navCtrl: NavController, public navParams: NavParams, public firebaseProvider:FirebaseProvider, public httpProvider:HttpProvider) {
  }

  addStreamer(){
    this.firebaseProvider.addStreamer(this.inputStreamer).then(() => this.navCtrl.pop());
  }

  searchChannels(){
    if(this.inputStreamer.length >= 3){
      this.foundChannels = this.httpProvider.searchChannels(this.inputStreamer, 5)
    } else {
      this.foundChannels = null;
    }
  }

  changeInput(channel:string){
    this.inputStreamer = channel;
    this.foundChannels = null;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddStreamerPage');
  }

}
