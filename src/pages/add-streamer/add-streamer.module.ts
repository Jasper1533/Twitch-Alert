import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddStreamerPage } from './add-streamer';

@NgModule({
  declarations: [
    AddStreamerPage,
  ],
  imports: [
    IonicPageModule.forChild(AddStreamerPage),
  ],
})
export class AddStreamerPageModule {}
