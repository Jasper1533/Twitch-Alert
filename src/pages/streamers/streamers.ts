import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { FirebaseProvider } from '../../providers/firebase/firebase';

/**
 * Generated class for the StreamersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-streamers',
  templateUrl: 'streamers.html',
})
export class StreamersPage {
  streamersOffline;
  streamersOnline;
  noStreamers = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, public httpProvider: HttpProvider, public firebaseProvider: FirebaseProvider) {
    this.getStreams();
  }

  ionViewDidEnter(){
    this.getStreams();
  }

  removeStreamer(streamer:string, index:number, status:string){
    this.firebaseProvider.removeStreamer(streamer);
    
      if(status == "online"){
        this.streamersOnline.splice(index, 1);
      } else if(status == "offline"){
        this.streamersOffline.splice(index, 1);
      }
      
      if(this.streamersOffline.length == 0 && this.streamersOnline.length == 0){
        this.noStreamers = true;
      }
    
  }

  goToAddStreamer(){
    this.navCtrl.push("AddStreamerPage");
  }

  goToStreamerDetail(streamerName:string){
    this.navCtrl.push("StreamerDetailPage", {streamerName: streamerName});
  }

   getStreams(){
    this.firebaseProvider.getStreamers().then( (data) => {
      if(data && data.length != 0){
        this.httpProvider.getStreamerDataFromList(data, 0).then((streamers) => {
          this.streamersOffline = streamers[0];
          this.streamersOnline = streamers[1];
          this.noStreamers = false;
          // setTimeout(() => { this.streamersOffline.sort(function(a, b) {
          //   console.log(3) // ignore upper and lowercase
          //   if (a.display_name < b.display_name) {
          //     return -1;
          //   }
          //   if (a.display_name > b.display_name) {
          //     return 1;
          //   }
          
          //   // names must be equal
          //   return 0;
          // });}, 3000);
          
        });
      } else {
        this.noStreamers = true;
      }
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StreamersPage');
  }

}
