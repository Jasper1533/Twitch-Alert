import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StreamersPage } from './streamers';

@NgModule({
  declarations: [
    StreamersPage,
  ],
  imports: [
    IonicPageModule.forChild(StreamersPage),
  ],
})
export class StreamersPageModule {}
