import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StreamerDetailPage } from './streamer-detail';

@NgModule({
  declarations: [
    StreamerDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(StreamerDetailPage),
  ],
})
export class StreamerDetailPageModule {}
