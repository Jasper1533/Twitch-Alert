import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the StreamerDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-streamer-detail',
  templateUrl: 'streamer-detail.html',
})
export class StreamerDetailPage {
  streamer;
  isOnline = false;
  stream: Observable<any>;
  channel: Observable<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public httpProvider: HttpProvider) {
    this.streamer = navParams.get('streamerName');
    this.channel = this.httpProvider.getChannel(this.streamer);
    this.getStream(this.streamer);
  }

  getStream(streamer:string){
    this.stream = this.httpProvider.getStream(streamer);

    this.stream.subscribe((streamData:any) => {
      this.checkStatus(streamData.stream)
    });
  }

  checkStatus(status){
    if(status == null){
      this.isOnline = false;
    } else {
      this.isOnline = true;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StreamerDetailPage');
  }

}
