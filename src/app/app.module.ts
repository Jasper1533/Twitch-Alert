import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { StorageProvider } from '../providers/storage/storage';
import { IonicStorageModule } from '@ionic/storage';
import { Firebase } from '@ionic-native/firebase';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { HttpProvider } from '../providers/http/http';
import { FirebaseProvider } from '../providers/firebase/firebase';

const firebase = {
  apiKey: "AIzaSyDk5f3nQx7N3wHMCsMg-h_rlPJFfqrnG50",
  authDomain: "twitch-alert-64acf.firebaseapp.com",
  databaseURL: "https://twitch-alert-64acf.firebaseio.com",
  projectId: "twitch-alert-64acf",
  storageBucket: "",
  messagingSenderId: "223494993374"
}


@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebase),
    AngularFirestoreModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    StorageProvider,
    Firebase,
    FirebaseProvider,
    HttpProvider
  ]
})
export class AppModule {}
