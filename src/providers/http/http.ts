import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the HttpProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
const headers = {
  'Client-ID':'o5ir677u7ysdtrm9g9yt79hbbllah2'
}
@Injectable()
export class HttpProvider {

  constructor(public http: HttpClient) {
    console.log('Hello HttpProvider Provider');
  }

  getStream(streamer:string){
    return this.http.get('https://api.twitch.tv/kraken/streams/' + streamer, {headers: headers});
  }

  getChannel(channel:string){
    return this.http.get('https://api.twitch.tv/kraken/channels/' + channel, {headers: headers});
  }

  async getStreamerDataFromList(storage:Array<any>, index:number, online:Array<any>=[], offline:Array<any>=[]){
    if(storage[index] !== undefined){
      let streamdata = this.getStream(storage[index]['username']);
      
      streamdata.subscribe((data:any) => {
        if(data.stream == null){
          let channeldata = this.getChannel(storage[index]['username']);
        
          channeldata.subscribe((data:any) => {
            offline.push(data);
          });
        } else {
          online.push(data["stream"]["channel"]);
        }
      });
      return this.getStreamerDataFromList(storage, index+1, online, offline);
    } else {
      return [offline, online]
    }
  }

  searchChannels(query:string, limit:number){
    return this.http.get('https://api.twitch.tv/kraken/search/channels?query=' + query + '&limit=' + limit, {headers: headers})
  }
}
