import { Injectable } from '@angular/core';
import { Firebase } from '@ionic-native/firebase';
import { Platform } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';

/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
const COLLECTION_NAME = 'streamers';

@Injectable()
export class FirebaseProvider {

  constructor(public firebaseNative: Firebase, public afs: AngularFirestore) {
    
  }

  listenToNotifications(){
    return this.firebaseNative.onNotificationOpen();
  }

  public addStreamer(streamer:string){
    streamer = streamer.toLowerCase();
    return this.afs.collection(COLLECTION_NAME).doc(streamer).set({username: streamer});
  }

  public removeStreamer(streamer:string){
    streamer = streamer.toLowerCase();
    return this.getStreamers().then((streamers:any) => {
      if(streamers){
        return this.afs.collection(COLLECTION_NAME).doc(streamer).delete();
      }
    })
  }

  public getStreamers(){
    let streamers = [];
    return this.afs.collection(COLLECTION_NAME).ref.get().then((data) => {
      data.forEach((doc) => {
        streamers.push(doc.data());
      })
      return streamers;
    });
  }

}
