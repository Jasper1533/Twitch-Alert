import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

/*
  Generated class for the StorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
const STORAGE_KEY = "streamers"

@Injectable()
export class StorageProvider {

  constructor(public http: HttpClient, public storage:Storage) {
    
  }

  public addStreamer(streamer:string){
    streamer = streamer.toLowerCase();
    return this.getStreamers().then((streamers) => {
      if(streamers){
        if(streamers.indexOf(streamer) == -1){
          streamers.push(streamer);
          return this.storage.set(STORAGE_KEY, streamers);
        }
      } else {
        return this.storage.set(STORAGE_KEY, [streamer]);
      }
    })
  }

  public removeStreamer(streamer:string){
    streamer = streamer.toLowerCase();
    return this.getStreamers().then((streamers) => {
      if(streamers){
        streamers.splice(streamers.indexOf(streamer), 1);
        return this.storage.set(STORAGE_KEY ,streamers);
      }
    })
  }

  public getStreamers(){
    return this.storage.get(STORAGE_KEY);
  }

}
